package pl.me.bitalinoclassifier.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import info.plux.pluxapi.bitalino.BITalinoException;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;
import pl.me.bitalinoclassifier.fragments.Tab1;

/**
 * Created by Piotr1 on 15.02.2018.
 */

public class BITalinoAcquisitionStopService extends IntentService {

    public BITalinoAcquisitionStopService(String name) {
        super(name);
    }

    public BITalinoAcquisitionStopService() {
        this("BITalinoAcquisitionStopService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            if (BITalinoHolder.bitalinoCommunication != null)
                BITalinoHolder.bitalinoCommunication.stop();
        } catch (BITalinoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        Tab1.toggleButton.setEnabled(true);
        Tab1.acquisitionToggleButton.setEnabled(true);
        super.onDestroy();
    }
}
