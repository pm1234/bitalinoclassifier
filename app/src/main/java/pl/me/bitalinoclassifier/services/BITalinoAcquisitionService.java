package pl.me.bitalinoclassifier.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import info.plux.pluxapi.bitalino.BITalinoException;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;
import pl.me.bitalinoclassifier.fragments.Tab1;

/**
 * Created by Piotr1 on 15.02.2018.
 */

public class BITalinoAcquisitionService extends IntentService {
    public BITalinoAcquisitionService(String name) {
        super(name);
    }

    public BITalinoAcquisitionService() {
        this("BITalinoAcquisitionService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (BITalinoHolder.bitalinoCommunication != null) {
            try {
                BITalinoHolder.bitalinoCommunication.start(new int[]{0,1,2},BITalinoHolder.SAMPLE_RATE);
            } catch (BITalinoException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        Tab1.toggleButton.setEnabled(true);
        Tab1.acquisitionToggleButton.setEnabled(true);
        System.out.println("Acquisition service destroyed.");
        super.onDestroy();
    }
}
