package pl.me.bitalinoclassifier.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import info.plux.pluxapi.bitalino.BITalinoException;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;

/**
 * Created by Piotr1 on 15.02.2018.
 */

public class BITalinoDisconnectingService extends IntentService {

    public BITalinoDisconnectingService(String name) {
        super(name);
    }

    public BITalinoDisconnectingService() {
        this("BITalinoDisconnectingService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            BITalinoHolder.bitalinoCommunication.stop();
        } catch (BITalinoException e) {  }
        try {
            BITalinoHolder.bitalinoCommunication.disconnect();
            BITalinoHolder.bitalinoCommunication = null;
        } catch (BITalinoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
//        Tab1.toggleButton.setEnabled(true);
//        Tab1.acquisitionToggleButton.setEnabled(false);
        super.onDestroy();
    }
}
