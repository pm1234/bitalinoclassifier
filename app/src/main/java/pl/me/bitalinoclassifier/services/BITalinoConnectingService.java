package pl.me.bitalinoclassifier.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;

import info.plux.pluxapi.Communication;
import info.plux.pluxapi.Constants;
import info.plux.pluxapi.bitalino.BITalinoCommunicationFactory;
import info.plux.pluxapi.bitalino.BITalinoException;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;
import pl.me.bitalinoclassifier.callbacks.BITalinoDataCallback;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class BITalinoConnectingService extends IntentService {
    public static final String PARAM_IN_MSG = "imsg";
    public static final String PARAM_OUT_MSG = "omsg";

    public BITalinoConnectingService(String name) {
        super(name);
    }

    public BITalinoConnectingService() {
        super("BITalino Service");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
//        String msg = intent.getStringExtra(PARAM_IN_MSG);
//        SystemClock.sleep(5000); // 30 seconds
//        String resultTxt = msg + " "
//                + DateFormat.format("MM/dd/yy h:mmaa", System.currentTimeMillis());
//        System.out.println(resultTxt);
//        // processing done here….
//        Intent broadcastIntent = new Intent();
//        broadcastIntent.setAction(ResponseReceiver.ACTION_RESP);
//        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
//        broadcastIntent.putExtra(PARAM_OUT_MSG, resultTxt);
//        sendBroadcast(broadcastIntent);
        BITalinoHolder.bitalinoCommunication =
                new BITalinoCommunicationFactory().getCommunication(Communication.BTH,
                        getApplicationContext(), new BITalinoDataCallback());
        try {
            int[] analogChannels = new int[]{0,1,2,3};
            int sampleRate = 10;
            BITalinoHolder.bitalinoCommunication.connect(BITalinoHolder.MAC_ADDRESS);
//            boolean ok = false;
//            while (!ok) {
//                try {
//                    Thread.sleep(2000);
//                    System.out.println(BITalinoHolder.bitalinoCommunication.state());
//                    ok = true;
//                } catch (Exception e) {
//                    System.err.println(e);
//                    ok = false;
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e1) {
//                        e1.printStackTrace();
//                    }
//                }
//            }
//            System.out.println(bitalinoCommunication.state());
//            Thread.sleep(10000);
//            BITalinoHolder.bitalinoCommunication.start(analogChannels,sampleRate);
        } catch (BITalinoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
//        BITalinoHolder.isConnected = true;
        System.out.println("Service destroyed.");
//        Tab1.toggleButton.setEnabled(true);
//        Tab1.acquisitionToggleButton.setEnabled(true);

//        super.onDestroy();
    }

    public static IntentFilter updateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_STATE_CHANGED);
        intentFilter.addAction(Constants.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(Constants.ACTION_COMMAND_REPLY);
        intentFilter.addAction(Constants.ACTION_MESSAGE_SCAN);
        intentFilter.addAction(Constants.ACTION_CONNECTED);
        return intentFilter;
    }
}
