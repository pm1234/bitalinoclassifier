package pl.me.bitalinoclassifier.callbacks;

import android.util.Log;

import info.plux.pluxapi.bitalino.BITalinoFrame;
import info.plux.pluxapi.bitalino.bth.OnBITalinoDataAvailable;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;

import static android.content.ContentValues.TAG;

/**
 * Created by Piotr1 on 15.02.2018.
 */

public class BITalinoDataCallback implements OnBITalinoDataAvailable {
    @Override
    public void onBITalinoDataAvailable(BITalinoFrame bitalinoFrame) {
//        Log.d(TAG, "BITalinoFrame: " + bitalinoFrame.toString());
        BITalinoHolder.buffer.put(
                bitalinoFrame.getAnalog(0),
                bitalinoFrame.getAnalog(1),
                bitalinoFrame.getAnalog(2),
                (short) bitalinoFrame.getSequence());
    }
}
