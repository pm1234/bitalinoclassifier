package pl.me.bitalinoclassifier.fragments;

/**
 * Created by Piotr1 on 12.02.2018.
 */

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.FastLineAndPointRenderer;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import pl.me.bitalinoclassifier.R;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;

public class Tab3 extends Fragment {
    /*
    The basic steps are:

    1.Create an instance of Series and populate it with data to be displayed.
    2.Register one or more series with the plot instance along with a Formatter to describing how the series should look when drawn.
    3.Draw the Plot

    Since we're working with XY data, we’ll use XYPlot, SimpleXYSeries (which is an implementation of the XYSeries interface)
    and LineAndPointFormatter
     */

    private static XYPlot ecgPlot, edaPlot, emgPlot;
    private static XYSeries ecgSeries, edaSeries, emgSeries;
    private static LineAndPointFormatter seriesFormat;
    private static Runnable plotRefresher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab3, container, false);

        ecgPlot = rootView.findViewById(R.id.plot1);
        edaPlot = rootView.findViewById(R.id.plot2);
        emgPlot = rootView.findViewById(R.id.plot3);

        ecgSeries = new SimpleXYSeries(
                new LinkedList<Number>(BITalinoHolder.buffer.getEcgValuesAsList()), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "ECG");
        edaSeries = new SimpleXYSeries(
                new LinkedList<Number>(BITalinoHolder.buffer.getEdaValuesAsList()), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "EDA");
        emgSeries = new SimpleXYSeries(
                new LinkedList<Number>(BITalinoHolder.buffer.getEmgValuesAsList()), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "EMG");

        seriesFormat = new FastLineAndPointRenderer.Formatter(Color.RED,null,null);
        seriesFormat.setLegendIconEnabled(false);
        seriesFormat.setPointLabeler(null);
        seriesFormat.getLinePaint().setAntiAlias(false);

        ecgPlot.addSeries(ecgSeries, seriesFormat);
        edaPlot.addSeries(edaSeries, seriesFormat);
        emgPlot.addSeries(emgSeries, seriesFormat);

//       ecgPlot.setRangeStep(StepMode.INCREMENT_BY_VAL,100);
       ecgPlot.setRangeBoundaries(null,null, BoundaryMode.AUTO);
        edaPlot.setRangeStep(StepMode.INCREMENT_BY_PIXELS,100);
        edaPlot.setRangeBoundaries(null,null, BoundaryMode.AUTO);
//        emgPlot.setRangeStep(StepMode.INCREMENT_BY_VAL,100);
        emgPlot.setRangeBoundaries(null,null, BoundaryMode.AUTO);

//        edaPlot.getLayoutManager().addToBottom(edaPlot.getRangeTitle());
//        edaPlot.getLayoutManager().addToBottom(edaPlot.getLegend());
        for (XYPlot plot : Arrays.asList(ecgPlot,edaPlot,emgPlot)) {
            plot.getLayoutManager().remove(plot.getLegend());
            plot.getLayoutManager().remove(plot.getDomainTitle());
            plot.getLayoutManager().remove(plot.getRangeTitle());
            plot.setPadding(0,0,0,0);
            plot.setPlotMargins(0,0,0,0);
            plot.getGraph().setPadding(50,30,0,10);
            plot.getGraph().setMargins(0,0,0,0);
        }
        plotRefresher = new PlotRefresher();

        return rootView;
    }

    private static class PlotRefresher implements Runnable {

        @Override
        public void run() {
            ecgPlot.clear();
            edaPlot.clear();
            emgPlot.clear();

            ecgSeries = new SimpleXYSeries(
                    new LinkedList<Number>(BITalinoHolder.buffer.getEcgValuesAsList()), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "ECG");
            edaSeries = new SimpleXYSeries(
                    new LinkedList<Number>(BITalinoHolder.buffer.getEdaValuesAsList()), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "EDA");
            emgSeries = new SimpleXYSeries(
                    new LinkedList<Number>(BITalinoHolder.buffer.getEmgValuesAsList()), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "EMG");

            ecgPlot.addSeries(ecgSeries, seriesFormat);
            edaPlot.addSeries(edaSeries, seriesFormat);
            emgPlot.addSeries(emgSeries, seriesFormat);

            ecgPlot.redraw();
            edaPlot.redraw();
            emgPlot.redraw();
            BITalinoHolder.handler.postDelayed(this,1000);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TAB3","onCreate()");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("TAB3","onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TAB3","onResume()");
        BITalinoHolder.handler.post(plotRefresher);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAB3","onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAB3","onStop()");
        BITalinoHolder.handler.removeCallbacks(plotRefresher);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TAB3","onDestroy()");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("TAB3","onViewCreated()");
    }
}
