package pl.me.bitalinoclassifier.fragments;

/**
 * Created by Piotr1 on 12.02.2018.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.ToggleButton;

import pl.me.bitalinoclassifier.R;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;
import pl.me.bitalinoclassifier.services.BITalinoAcquisitionService;
import pl.me.bitalinoclassifier.services.BITalinoAcquisitionStopService;
import pl.me.bitalinoclassifier.services.BITalinoConnectingService;
import pl.me.bitalinoclassifier.services.BITalinoDisconnectingService;

public class Tab1 extends Fragment {
    public static ToggleButton toggleButton;
    public static ToggleButton acquisitionToggleButton;
    public static ProgressBar batteryStatus;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.tab1, container, false);

        Log.d("TAG","onCreateView() - Tab1");

//        toggleButtonConn = container.findViewById(R.id.toggleButtonConn);
//        toggleButtonConn = getActivity().findViewById(R.id.toggleButtonConn);
        toggleButton = rootView.findViewById(R.id.toggleButtonConn);
        acquisitionToggleButton = rootView.findViewById(R.id.toggleButtonAcquis);
        batteryStatus = rootView.findViewById(R.id.batteryProgressBar);
        final Intent connectionIntent = new Intent(getContext(), BITalinoConnectingService.class);
        final Intent disconnectionIntent = new Intent(getContext(), BITalinoDisconnectingService.class);
        final Intent acquisitionIntent = new Intent(getContext(), BITalinoAcquisitionService.class);
        final Intent acquisitionStopIntent = new Intent(getContext(), BITalinoAcquisitionStopService.class);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
//                    getActivity().startService(intent);
//                    msgIntent.putExtra(BITalinoConnectingService.PARAM_IN_MSG, "strInputMsg");
//                    getActivity().startService(msgIntent);
                    if (BITalinoHolder.bitalinoCommunication == null) {
                        getActivity().startService(connectionIntent);
                        getActivity().registerReceiver(BITalinoHolder.receiver,BITalinoConnectingService.updateIntentFilter());
//                        Snackbar.make(container, "Connecting started", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                        toggleButton.setEnabled(false);
                    }
                }
                else {
                    if (BITalinoHolder.bitalinoCommunication != null) {
                        getActivity().startService(disconnectionIntent);
                        acquisitionToggleButton.setChecked(false);
                        acquisitionToggleButton.setEnabled(false);
                        batteryStatus.setProgress(0);
                        try {
                            getActivity().unregisterReceiver(BITalinoHolder.receiver);
                        } catch (IllegalArgumentException e) {
                            Log.w("TAB1",e.getMessage());
                        }
                    }
                }
            }
        });

        acquisitionToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (BITalinoHolder.bitalinoCommunication != null) {
                        try {
                            getActivity().startService(acquisitionIntent);
                            acquisitionToggleButton.setEnabled(false);
//                            Snackbar.make(container, "Acquiring started", Snackbar.LENGTH_LONG)
//                                    .setAction("Action", null).show();
                        } catch (Exception e) {
                            System.err.println(e.getMessage());
                        }

                    }
                }
                else {
                    if (BITalinoHolder.bitalinoCommunication != null) {
                        getActivity().startService(acquisitionStopIntent);
                        acquisitionToggleButton.setEnabled(false);
                    }
                }
            }
        });
//        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
//        filter.addCategory(Intent.CATEGORY_DEFAULT);
//        receiver = new ResponseReceiver();
//        getActivity().registerReceiver(receiver, filter);
        if (BITalinoHolder.bitalinoCommunication != null)
            acquisitionToggleButton.setEnabled(true);
        return rootView;
    }

    @Override
    public void onPause() {
        Log.d("TAG","onPause() - Tab1");
        try {
            getActivity().unregisterReceiver(BITalinoHolder.receiver);
        } catch (IllegalArgumentException e) {
            Log.w("TAB1","Receiver was not registered.");
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d("TAG","onStop() - Tab1");
        super.onStop();
    }

    @Override
    public void onResume() {
        Log.d("TAG","onResume() - Tab1");
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        Log.d("TAG","onDestroyView() - Tab1");
        super.onDestroyView();
    }
}
