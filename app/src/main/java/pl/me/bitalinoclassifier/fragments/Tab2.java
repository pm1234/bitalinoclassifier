package pl.me.bitalinoclassifier.fragments;

/**
 * Created by Piotr1 on 12.02.2018.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.jannlab.data.Sample;
import pl.me.bitalinoclassifier.R;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;
import pl.me.bitalinoclassifier.assist.FeatureExtractor;
import pl.me.bitalinoclassifier.assist.RingBufferFlipMarker;
import pl.me.bitalinoclassifier.neural_network.NeuralNetwork;

public class Tab2 extends Fragment {
    private Button
            joyButton, gladnessButton, excitementButton,
            noEmotionButton, artifactButton,
            stressButton, angerButton, sadnessButton,
            testButton, saveNetsButton;
    private AlertDialog.Builder alertDialog;
    private static final String DATASET_FILENAME = Environment.getExternalStorageDirectory().getAbsolutePath()+"/mgr/dataset.txt";
    private static final String PATH_CORE = Environment.getExternalStorageDirectory().getAbsolutePath()+"/mgr/";
    private static RingBufferFlipMarker buffer = BITalinoHolder.buffer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab2, container, false);
        Log.d("TAG","onCreateView() - Tab2");

        testButton = rootView.findViewById(R.id.button_test);
        joyButton = rootView.findViewById(R.id.button_joy);
        gladnessButton = rootView.findViewById(R.id.button_gladness);
        excitementButton = rootView.findViewById(R.id.button_excitement);
        noEmotionButton = rootView.findViewById(R.id.button_neutral);
        artifactButton = rootView.findViewById(R.id.button_artifact);
        stressButton = rootView.findViewById(R.id.button_stress);
        angerButton = rootView.findViewById(R.id.button_anger);
        sadnessButton = rootView.findViewById(R.id.button_sadness);
        saveNetsButton = rootView.findViewById(R.id.button_save_nets);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            alertDialog = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            alertDialog = new AlertDialog.Builder(getContext());
        }

        testButton.setOnClickListener(testButtonListener);
        joyButton.setOnClickListener(joyButtonListener);
        gladnessButton.setOnClickListener(gladnessButtonListener);
        excitementButton.setOnClickListener(excitementButtonListener);
        noEmotionButton.setOnClickListener(noEmotionButtonListener);
        artifactButton.setOnClickListener(artifactButtonListener);
        stressButton.setOnClickListener(stressButtonListener);
        angerButton.setOnClickListener(angerButtonListener);
        sadnessButton.setOnClickListener(sadnessButtonListener);
        saveNetsButton.setOnClickListener(saveNetsButtonListener);

        rootView.findViewById(R.id.scroll_view).setVerticalScrollBarEnabled(true);

        return rootView;
    }

    // - - - - - - - Buttons listeners - - - - - - //
    private View.OnClickListener testButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int [][] transposedData = buffer.transposeSamplesBuffer(buffer.getNewestSamples(BITalinoHolder.TIME_WINDOW_LENGTH));
            //here signals themselves are stored and pushed to the feature extractor
            //FeatureExtractor object computes the derivatives of EDA and EMG
            int[] ecgValues = transposedData[0];
            int[] edaValues = transposedData[1];
            int[] emgValues = transposedData[2];

            //array containing time series
//            double[] timeSeriesArray = new double[ecgValues.length*3];
//            int i;
//            for (i=0; i<ecgValues.length; i++)
//                timeSeriesArray[i] = ecgValues[i];
//            for (int j=0; i<ecgValues.length+edaValues.length; j++, i++)
//                timeSeriesArray[i] = edaValues[j];
//            for (int j=0; i<timeSeriesArray.length; j++, i++)
//                timeSeriesArray[i] = emgValues[j];

            //array containing features
            double[] featuresArray = new double[BITalinoHolder.FEATURES_AMOUNT];
            //array containing time series and features
//            double[] timeSeriesFeaturesArray = new double[ecgValues.length*3+BITalinoHolder.FEATURES_AMOUNT];

//            System.arraycopy(timeSeriesArray,0,timeSeriesFeaturesArray,0,timeSeriesArray.length);
            try {
                FeatureExtractor fe = new FeatureExtractor(ecgValues,edaValues,emgValues);
                featuresArray[0] = fe.countEdaPeaks(0);
                featuresArray[1] = fe.countEmgPeaks(515); //515 is the threshold for EMG signal (not for the derivative)
                featuresArray[2] = fe.getMinHeartRate(900); //900 is the threshold for calculating HR from ECG (ECG peaks are calculated first)
                featuresArray[3] = fe.getMaxHeartRate(900);
                featuresArray[4] = fe.getMeanHeartRate(900);
                featuresArray[5] = fe.getHeartRateVar(900);
                featuresArray[6] = fe.getEdaDerivativeIntegral();
                featuresArray[7] = fe.getEmgDerivativeIntegral();
                featuresArray[8] = fe.getEdaTrend();
//            featuresArray[9] = fe.getEmgPeaksLocsMean(500); //500 is the threshold for EMG signal for detecting noise
//            featuresArray[10] = fe.getEmgPeaksLocsVar(500);
                featuresArray[9] = fe.getHeartRateTrend();
                featuresArray[10] = fe.getEdaZeroSamplesCount();
                featuresArray[11] = fe.getEmgPeaksMean(500);
                featuresArray[12] = fe.getEmgPeaksVar(500);
                featuresArray[13] = fe.getEcgFlatSamplesCount();
                featuresArray[14] = fe.getEdaDerivMin();
                featuresArray[15] = fe.getEdaDerivMax();


//            System.arraycopy(featuresArray,0,timeSeriesFeaturesArray,timeSeriesArray.length,featuresArray.length);
                StringBuilder sb = new StringBuilder();

                for (NeuralNetwork nn : BITalinoHolder.neuralNetworks) {
                    Sample testSample;
//                if (nn.getName().contains("_TS_"))
//                    testSample = new Sample(timeSeriesArray,null);
//                else if (nn.getName().contains("_F_"))
                    testSample = new Sample(featuresArray,new double[]{0.5});
//                else
//                    testSample = new Sample(timeSeriesFeaturesArray, null);
                    sb.append(nn.getName()).append(": ").append(String.format(Locale.ENGLISH,"%.2f",nn.testWithLiveData(testSample))).append("\n");
                }

                alertDialog.setMessage(sb.toString());
                alertDialog.setCancelable(true);
                alertDialog.setNeutralButton(
                        "Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                alertDialog.create().show();
            } catch (Exception e) {
                alertDialog.setMessage("Error while extracting features: "+e);
                alertDialog.setCancelable(true);
                alertDialog.setNeutralButton(
                        "Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                alertDialog.create().show();
                e.printStackTrace();
            }
        }
    };

    private View.OnClickListener joyButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new NNTrainerTask(getContext()).execute("joy");
        }
    };
    private View.OnClickListener gladnessButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new NNTrainerTask(getContext()).execute("gladness");
        }
    };
    private View.OnClickListener excitementButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new NNTrainerTask(getContext()).execute("excitement");
        }
    };
    private View.OnClickListener noEmotionButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new NNTrainerTask(getContext()).execute("no_emotion");
        }
    };
    private View.OnClickListener artifactButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new NNTrainerTask(getContext()).execute("artifact");
        }
    };
    private View.OnClickListener stressButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new NNTrainerTask(getContext()).execute("stress");
        }
    };
    private View.OnClickListener angerButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new NNTrainerTask(getContext()).execute("anger");
        }
    };
    private View.OnClickListener sadnessButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new NNTrainerTask(getContext()).execute("sadness");
        }
    };
    private View.OnClickListener saveNetsButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            for (NeuralNetwork nn : BITalinoHolder.neuralNetworks) {

                nn.saveNet(PATH_CORE+nn.getName()+".net");
                nn.saveNetWeights(PATH_CORE+nn.getName()+"_weights.txt");
            }
        }
    };

    /**
     * Static inner class implementing asynchronuous task responsible for processing data and training nets after the user clicks a button
     */
    private static class NNTrainerTask extends AsyncTask<String, String, Void> {
        private int step;
        private final int STEPS_COUNT = 2+BITalinoHolder.neuralNetworks.size();
        private ProgressDialog progressDialog;

        private NNTrainerTask(Context context) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        }


        @Override
        protected void onPreExecute() {
            progressDialog.setProgress(0);
            progressDialog.setTitle("Training...");
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            publishProgress("Downloading data...");
            int[][] data = buffer.transposeSamplesBuffer(buffer.getOldestSamples(BITalinoHolder.TIME_WINDOW_LENGTH));
            publishProgress("Saving data to the file...");
            saveCurrentCaseInFile(data, strings[0]);
            publishProgress("Preparing train cases...");
            prepareAndSaveCases(data,strings[0]);
            List<NeuralNetwork> neuralNetworks = BITalinoHolder.neuralNetworks;
            if (!validateSetsSizes()) {
                Log.e("trainAndSaveDataset()","Training sets inconsistency!");
            }
            for (int i = 0; i < neuralNetworks.size(); i++) {
                NeuralNetwork nn = neuralNetworks.get(i);
                publishProgress("Training net no. "+(i+1)+"...");
                nn.trainNetAndSaveDataset();
//                nn.saveDataset();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(100*(step-1)/STEPS_COUNT);
            progressDialog.setMessage(values[0]);
            progressDialog.setTitle(values[0]);
            step++;
        }
    };

    private static void saveCurrentCaseInFile(int[][] transposedData, String emotion) {
        BufferedWriter bw = null;
        try {
            File picDirectory = new File(PATH_CORE);
            if (!picDirectory.exists()) {
                picDirectory.mkdirs();
            }
            bw = new BufferedWriter(new FileWriter(new File(DATASET_FILENAME),true));
            int[] ecgValues = transposedData[0];
            int[] edaValues = transposedData[1];
            int[] emgValues = transposedData[2];
            int[] inputArray = new int[ecgValues.length*3];
            System.arraycopy(ecgValues,0,inputArray,0,ecgValues.length);
            System.arraycopy(edaValues,0,inputArray,ecgValues.length,edaValues.length);
            System.arraycopy(emgValues,0,inputArray,ecgValues.length+edaValues.length,emgValues.length);
//            Log.d("DatasetSaving",Arrays.toString(inputArray)+","+emotion);
            bw.write(String.valueOf(new Date().getTime())+",");
            bw.write(Arrays.toString(inputArray)+","+emotion);
            bw.write('\n');
        } catch (IOException e) {
            Log.e("DatasetSaving","Error while saving data case in text file: "+e.getMessage());
        } finally {
            try {
                assert bw != null;
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void prepareAndSaveCases(int[][] transposedData, String emotion) {
        //here signals themselves are stored and pushed to the feature extractor
        //FeatureExtractor object computes the derivatives of EDA and EMG
        int[] ecgValues = transposedData[0];
        int[] edaValues = transposedData[1];
        int[] emgValues = transposedData[2];

        //array containing time series
//        double[] timeSeriesArray = new double[ecgValues.length*3];
//        int i;
//        for (i=0; i<ecgValues.length; i++)
//            timeSeriesArray[i] = ecgValues[i];
//        for (int j=0; i<ecgValues.length+edaValues.length; j++, i++)
//            timeSeriesArray[i] = edaValues[j];
//        for (int j=0; i<timeSeriesArray.length; j++, i++)
//            timeSeriesArray[i] = emgValues[j];

        //array containing features
        double[] featuresArray = new double[BITalinoHolder.FEATURES_AMOUNT];
        //array containing time series and features
//        double[] timeSeriesFeaturesArray = new double[ecgValues.length*3+BITalinoHolder.FEATURES_AMOUNT];

//        System.arraycopy(timeSeriesArray,0,timeSeriesFeaturesArray,0,timeSeriesArray.length);

        FeatureExtractor fe = new FeatureExtractor(ecgValues,edaValues,emgValues);
        featuresArray[0] = fe.countEdaPeaks(0);
        featuresArray[1] = fe.countEmgPeaks(515); //520 is the threshold for EMG signal (not for the derivative)
        featuresArray[2] = fe.getMinHeartRate(900); //900 is the threshold for calculating HR from ECG (ECG peaks are calculated first)
        featuresArray[3] = fe.getMaxHeartRate(900);
        featuresArray[4] = fe.getMeanHeartRate(900);
        featuresArray[5] = fe.getHeartRateVar(900);
        featuresArray[6] = fe.getEdaDerivativeIntegral();
        featuresArray[7] = fe.getEmgDerivativeIntegral();
        featuresArray[8] = fe.getEdaTrend();
        //            featuresArray[9] = fe.getEmgPeaksLocsMean(500); //500 is the threshold for EMG signal for detecting noise
        //            featuresArray[10] = fe.getEmgPeaksLocsVar(500);
        featuresArray[9] = fe.getHeartRateTrend();
        featuresArray[10] = fe.getEdaZeroSamplesCount();
        featuresArray[11] = fe.getEmgPeaksMean(500);
        featuresArray[12] = fe.getEmgPeaksVar(500);
        featuresArray[13] = fe.getEcgFlatSamplesCount();
        featuresArray[14] = fe.getEdaDerivMin();
        featuresArray[15] = fe.getEdaDerivMax();

//        System.arraycopy(featuresArray,0,timeSeriesFeaturesArray,timeSeriesArray.length,featuresArray.length);

        double positiveValue, neutralValue, negativeValue;
        switch (emotion) {
            case "joy":
            case "gladness":
            case "excitement":
                positiveValue = 1;
                neutralValue = negativeValue = 0;
                break;
            case "no_emotion":
            case "artifact":
                neutralValue = 1;
                positiveValue = negativeValue = 0;
                break;
            default:
                negativeValue = 1;
                positiveValue = neutralValue = 0;
                break;
        }

        double classValue;

        for (NeuralNetwork n : BITalinoHolder.neuralNetworks) {
            //setting appropriate class value for the current network
            if (n.getName().endsWith("POS"))
                classValue = positiveValue;
            else if (n.getName().endsWith("NEU"))
                classValue = neutralValue;
            else
                classValue = negativeValue;

            //setting appropriate input values for the current network
//            if (n.getName().contains("_TS_")) { // for nets taking only time series as input
//                n.addCaseToTrainingSet(new Sample(timeSeriesArray, new double[]{classValue}));
//            }
//            else if (n.getName().contains("_F_")) { // for nets taking only features as input
            n.addCaseToTrainingSet(new Sample(featuresArray, new double[]{classValue}));
//            }
//            else { // for nets taking both time series and features as input
//                n.addCaseToTrainingSet(new Sample(timeSeriesFeaturesArray, new double[]{classValue}));
//            }
        }
    }

    private static boolean validateSetsSizes() {
        int firstSize = BITalinoHolder.neuralNetworks.get(0).getTrainingSet().size();
        for (NeuralNetwork nn : BITalinoHolder.neuralNetworks) {
            if (nn.getTrainingSet().size() != firstSize) {
                Log.e("validateSetsSize()","Net "+nn.getName()+": training set size: "+nn.getTrainingSet().size()+", first size="+firstSize);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onPause() {
        Log.d("TAG","onPause() - Tab2");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d("TAG","onStop() - Tab2");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.d("TAG","onDestroyView() - Tab2");
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        Log.d("TAG","onResume() - Tab2");

        super.onResume();
    }
}
