package pl.me.bitalinoclassifier.assist;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Piotr1 on 17.02.2018.
 */

public class RingBufferFlipMarker {
    private short[] sampleValues;
    private int[] ecgValues;
    private int[] edaValues;
    private int[] emgValues;

    private int capacity = 0;
    private int writePos = 0;
    private int readPos  = 0;
//    private boolean flipped = false;   //the flip marker

    public RingBufferFlipMarker(int capacity) {
        this.capacity = capacity;
        sampleValues = new short[capacity];
        ecgValues = new int[capacity];
        edaValues = new int[capacity];
        emgValues = new int[capacity];
    }

    public void reset() {
        this.writePos = 0;
        this.readPos  = 0;
//        this.flipped  = false;
    }

    /*public int available() {
        if(!flipped){
            return writePos - readPos;
        }
        return capacity - readPos + writePos;
    }*/

    /*public int remainingCapacity() {
        if(!flipped){
            return capacity - writePos;
        }
        return readPos - writePos;
    }*/

    public synchronized boolean put(int ecgValue, int edaValue, int emgValue, short sampleValue){
        sampleValues[writePos] = sampleValue;
        ecgValues[writePos] = ecgValue;
        edaValues[writePos] = edaValue;
        emgValues[writePos] = emgValue;
        writePos = (writePos+1)%capacity;
        if (readPos == writePos) {
            readPos = (readPos+1)%capacity;
        }
        return true;
    }


    private int[] take() {
        int[] toReturn = new int[4];
        if (readPos == writePos) return null;
        toReturn[0] = ecgValues[readPos];
        toReturn[1] = edaValues[readPos];
        toReturn[2] = emgValues[readPos];
        toReturn[3] = sampleValues[readPos];
        readPos = (readPos+1)%capacity;
        return toReturn;
    }

    /**
     * debug method
     */
    public void printBuffer() {
        System.out.println("Samples: "+ Arrays.toString(sampleValues));
        System.out.println("ECG: "+ Arrays.toString(ecgValues));
        System.out.println("EDA: "+ Arrays.toString(edaValues));
        System.out.println("EMG: "+ Arrays.toString(emgValues));
        Short no;
        int[] ret;
        do {
            ret = take();
            if (ret != null) {
                no = (short) ret[3];
                System.out.println(no);
            }
        } while (ret != null);
    }

    /**
     * Method returning the oldest samples from the buffer (used to train the nets).
     * The first elements is the set of samples for which the ECG signal has the S-point
     * @param timeWindowLength legnth (in seconds) of the time frame that is used by the nets
     * @return 2D array with samples; 1st column contains ECG samples, 2nd - EDA samples, 3rd - EMG samples, 4th - sample numbers
     */
    public synchronized int[][] getOldestSamples(int timeWindowLength) {
        //find the lowest local extremum in ECG so that all samples sets will begin from the same phase
        int[] tempBuf = new int[BITalinoHolder.SAMPLE_RATE*3]; //3 seconds are enough
        if (capacity-readPos-1 < tempBuf.length) {// if the area from readPos to the end of the array is lower that tempBuf size
            System.arraycopy
                    (ecgValues,readPos,tempBuf,0,
                            capacity-readPos-1); // copy elements from the readPos index to the end of the ecgValues array
            System.arraycopy
                    (ecgValues,0,tempBuf,capacity-readPos-1,
                            tempBuf.length-(capacity-readPos-1)); //copy elements from the beginning of the ecgValues array

        }
        else { //if no flip is required
            System.arraycopy(ecgValues,readPos,tempBuf,0,tempBuf.length);
        }
//        FeatureExtractor fe = new FeatureExtractor();
        int idx = getIndexOfMinValue(tempBuf); //get index of the S-point
        readPos = (readPos+idx)%capacity; //move the readPos index respectively
        //so that the take() method will read proper elements

        int[][] data = new int[BITalinoHolder.SAMPLE_RATE*timeWindowLength][4];
        for (int i=0; i<data.length; i++) {
            data[i] = take();
            if (data[i] == null)
                Log.w("BITalinoClassifier", "Due to not enough data in the buffer sample no. "+(i+1)+" is null");
        }
        return data;
    }

    /**
     * Method returning the newest samples from the buffer (used to ask the nets about the emotion)
     * @param timeWindowLength legnth (in seconds) of the time frame that is used by the nets
     * @return 2D array with samples; 1st column contains ECG samples, 2nd - EDA samples, 3rd - EMG samples, 4th - sample numbers
     */
    public synchronized int[][] getNewestSamples(int timeWindowLength) {
        int[][] data = new int[BITalinoHolder.SAMPLE_RATE*timeWindowLength][4];
        int tempPointer;
        for (int i=data.length-1; i>-1; i--) {
            tempPointer = writePos+i-data.length-1;
            if (tempPointer < 0) tempPointer += capacity;
            data[i] = new int[]
                    {
                            ecgValues[tempPointer],
                            edaValues[tempPointer],
                            emgValues[tempPointer],
                            sampleValues[tempPointer]
                    };
        }
        readPos = writePos;
        return data;
    }

    /**
     * Method returning transposed form of the samples set (one row contains samples of one signal type)
     * @param samples buffer with the form where features are stored in separate columns
     * @return transposed buffer (one row contains samples of one signal type: 1st row - ECG, 2nd row - EDA, 3rd row - EMG, 4th row - sample numbers)
     */
    public int[][] transposeSamplesBuffer(int[][] samples) {
        return transposeDataMatrix(samples);
    }

    private int[][] transposeDataMatrix(int[][] data) {
        int dataRows = data.length;
        int dataCols = data[0].length;
        int[][] result = new int[dataCols][dataRows];
        for (int i=0; i<dataRows; i++) {
            for (int j=0; j<dataCols; j++) {
                result[j][i] = data[i][j];
            }
        }
        return result;
    }

    /**
     * Method finding the index of the so called S-point
     * @param series the ECG time series
     * @return index of the S-point (the lowest value within 1 period)
     */
    private int getIndexOfMinValue(int[] series) {
        int minVal = 0; //0 applies for the derivative of ECG signal
        int minValInd = 0;
        int dist = 0; //no. of samples from the last minimum
        for (int i = 0; i < series.length; i++) {
            if (series[i] < minVal) {
                minVal = series[i];
                minValInd = i;
                dist = 0;
            }
            else {
                dist++;
                if (dist > BITalinoHolder.SAMPLE_RATE*0.4) { //for HR=120 the next S-point will appear after 0.5 second
                    break;
                }
            }
        }
        return minValInd;
    }

    public List<Integer> getEcgValuesAsList() {
        ArrayList<Integer> ecg = new ArrayList<>(capacity);
        for (int e : ecgValues)
            ecg.add(e);
        return ecg;
    }

    public List<Integer> getEdaValuesAsList() {
        ArrayList<Integer> eda = new ArrayList<>(capacity);
        for (int e : edaValues)
            eda.add(e);
        return eda;
    }

    public List<Integer> getEmgValuesAsList() {
        ArrayList<Integer> emg = new ArrayList<>(capacity);
        for (int e : emgValues)
            emg.add(e);
        return emg;
    }
}
