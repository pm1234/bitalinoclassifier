package pl.me.bitalinoclassifier.assist;

import info.plux.pluxapi.bitalino.BITalinoException;

/**
 * Created by Piotr1 on 16.02.2018.
 */

public class BatteryCheckThread implements Runnable {
    @Override
    public void run() {
        try {
            if (BITalinoHolder.bitalinoCommunication != null)
                BITalinoHolder.bitalinoCommunication.state();
//            BITalinoHolder.handler.postDelayed(this,60000);
        } catch (BITalinoException e) {
            e.printStackTrace();
        }
    }
}
