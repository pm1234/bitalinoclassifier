package pl.me.bitalinoclassifier.assist;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Piotr1 on 19.02.2018.
 *
 */

public class FeatureExtractor {
    /*
    Table 1. Features used for stress detection
EDA                                                 Heart rate
Mean, SD, Variance, Max, # Peaks, Mean amplitude,   Mean, SD, Variance, Max
Mean rise time
Fourier feature
     */

    private int[] ecgValues, edaValues, emgValues;
    private int[] edaDerivative, emgDerivative;
    private int[] heartRateSamples;
    private int[] ecgPeaksLocation, edaPeaksLocation, emgPeaksLocation;
    private double[] heartRateMeanAndVar = null;
    private double[] emgPeaksMeanAndVar;
    private double[] emgPeaksLocsMeanAndVar;
    //private Integer minEdaValue, maxEdaValue;
    private static final short EPSILON = 5;
    private double[] edaDerivMinAndMax;

    public FeatureExtractor(int[] ecgValues, int[] edaValues, int[] emgValues) {
        this.ecgValues = ecgValues;
        this.edaValues = Filters.lowPassFilter(edaValues,0.9);
        this.emgValues = emgValues;
        edaDerivative = computeDerivative(edaValues);
        emgDerivative = computeDerivative(emgValues);
    }

    /**
     * Method computing derivative of given time series
     * @param series time series
     * @return 1D array containing the derivative of the given series
     */
    @org.jetbrains.annotations.Contract(pure = true)
    private int[] computeDerivative(int[] series) {
//        throw new UnsupportedOperationException("Not implemented yet!");
        int[] derivative = new int[series.length];
        for (int i=1; i<series.length-1; i++)
            derivative[i-1] = (int) ((series[i+1] - series[i-1])/((double)2/BITalinoHolder.SAMPLE_RATE));
        derivative[derivative.length-1] = derivative[derivative.length-2]; //so that the derivative series has the same length
        return derivative;
    }

    /**
     * Helper method calculating peaks (i.e. local maxima) in the given series. The point series[i] is defined as a local maximum iff
     * series[i] > series[i-1] && series[i] >= series[i+1]. It makes that in case of flat fragments with maximal value only first point
     * of such a fragment is considered to be a maximum
     * @param series 1D array containing time series
     * @param threshold the minimal value of the local maximum that will be taken into consideration
     * @param minDistance minimal distance between two samples containing local maximal values
     * @return 1D array containing indices of elements in the series array which are the local maxima
     */
    private int[] computePeaksLocation(int[] series, int threshold, int minDistance) {
        List<Integer> peaks = new LinkedList<>();
        int dist = 0;
        for (int i=1; i<series.length-1; i++) {
            dist++;
            if (series[i] > series[i-1] && series[i] >= series[i+1] && series[i] > threshold && dist >= minDistance) {
                //only one inequality is not sharp to avoid flat peaks (not relevant while processing the derivative of ECG)
                peaks.add(i);
                dist = 0;
            }
        }
        int[] toReturn = new int[peaks.size()];
        for (int i=0; i<toReturn.length; i++) {
            toReturn[i] = peaks.get(i);
        }
        return toReturn;
    }

    /**
     * Method counting local maxima in the EDA signal having greater value that the given threshold
     * @param threshold threshold for local maxima value (only greater values are considered)
     * @return number of local maxima
     */
    public int countEdaPeaks(int threshold) {
        //optionally, the derivative may be used
        if (edaPeaksLocation == null)
            edaPeaksLocation = computePeaksLocation(edaValues,threshold,BITalinoHolder.SAMPLE_RATE/2);
        return edaPeaksLocation.length;
    }

    /**
     * Method counting local maxima in the EMG signal having greater value that the given threshold
     * @param threshold threshold for local maxima value (only greater values are considered)
     * @return number of local maxima
     */
    public int countEmgPeaks(int threshold) {
//        if (emgPeaksLocation == null)
            emgPeaksLocation = computePeaksLocation(emgValues, threshold,0);
        return emgPeaksLocation.length;
    }

/*    public int getMinEdaValue(int[] edaSeries) {
        if (minEdaValue == null)
            calculateMinAndMaxEda(edaSeries);
        return minEdaValue;
    }*/

/*    public int getMaxEdaValue(int[] edaSeries) {
        if (maxEdaValue == null)
            calculateMinAndMaxEda(edaSeries);
        return maxEdaValue;
    }*/

/*    private void calculateMinAndMaxEda(int[] edaSeries) {
        minEdaValue = edaSeries[0];
        maxEdaValue = edaSeries[0];
        for (int eda : edaSeries) {
            if (eda < minEdaValue) {
                minEdaValue = eda;
            }
            else if (eda > maxEdaValue) {
                maxEdaValue = eda;
            }
        }
    }*/

    /**
     * Method computing HR values by using distance between two consecutive ECG peaks
     * @param threshold minimal value of peaks which will be considered while computing HR
     * @return 1D array with HR values; the length of the array is 1 less than amount of ECG peaks
     */
    private int[] getHeartRateSamples(int threshold) {
//        throw new UnsupportedOperationException("Not implemented yet!");
        if (heartRateSamples != null) return heartRateSamples;
        if (ecgPeaksLocation == null)
            ecgPeaksLocation = computePeaksLocation(ecgValues,threshold,0);
        int[] hr = new int[ecgPeaksLocation.length-1];
        for (int i=0; i<hr.length; i++) {
            hr[i] = 60*BITalinoHolder.SAMPLE_RATE/(ecgPeaksLocation[i+1] - ecgPeaksLocation[i]);
        }
        return (heartRateSamples = hr);
    }

    /**
     *
     * @param threshold
     * @return
     */
    private double[] getHeartRateMeanAndVar(int threshold) {
        if (heartRateSamples == null)
            heartRateSamples = getHeartRateSamples(threshold);
        double mean = 0;
        double var = 0;
        for (int i=0; i<heartRateSamples.length; i++) {
            mean += heartRateSamples[i];
            var += heartRateSamples[i]*heartRateSamples[i];
        }
        //currently, mean contains the sum and var contains squares sum
        if (heartRateSamples.length == 0) {
            mean = var = 0;
        }
        else {
            mean /= heartRateSamples.length;
            var = var/heartRateSamples.length - mean*mean;
        }
        return new double[]{mean, var};
//        return (heartRateMeanAndVar = getPeakMeanAndVar(heartRateSamples,0));
    }

    /**
     *
     * @param threshold
     * @return
     */
    public double getMeanHeartRate(int threshold) {
        if (heartRateMeanAndVar == null) {
            heartRateMeanAndVar = getHeartRateMeanAndVar(threshold);
        }
        return heartRateMeanAndVar[0];
    }

    /**
     *
     * @param threshold
     * @return
     */
    public double getHeartRateVar(int threshold) {
        if (heartRateMeanAndVar == null) {
            heartRateMeanAndVar = getHeartRateMeanAndVar(threshold);
        }
        return heartRateMeanAndVar[1];
    }

    /**
     *
     * @param threshold
     * @return
     */
    public int getMaxHeartRate(int threshold) {
        if (heartRateSamples == null)
            heartRateSamples = getHeartRateSamples(threshold);
        int max = 0;
        for (int h : heartRateSamples) {
            max = Math.max(max,h);
        }
        return max;
    }

    /**
     *
     * @param threshold
     * @return
     */
    public int getMinHeartRate(int threshold) {
        if (heartRateSamples == null)
            heartRateSamples = getHeartRateSamples(threshold);
        int min = 200;
        for (int h : heartRateSamples) {
            min = Math.min(min,h);
        }
        return min;
    }

    /**
     *
     * @return
     */
    private double[] getEdaDerivativeMinAndMax() {
        int max = 0, min = 0;
        for (int s : edaDerivative) {
            max = Math.max(max,s);
            min = Math.min(min,s);
        }
        return (edaDerivMinAndMax = new double[]{min,max});
    }

    /**
     *
     * @return
     */
    public double getEdaDerivMin() {
        if (edaDerivMinAndMax == null) {
            getEdaDerivativeMinAndMax();
        }
        return edaDerivMinAndMax[0];
    }

    /**
     *
     * @return
     */
    public double getEdaDerivMax() {
        if (edaDerivMinAndMax == null) {
            getEdaDerivativeMinAndMax();
        }
        return edaDerivMinAndMax[1];
    }

    /**
     *
     * @param series
     * @return
     */
    private double getAbsDerivativeIntegral(int[] series) {
        double acc = 0;
        for (int s : series)
            acc += Math.abs(s);
        return acc;
    }

    /**
     *
     * @return
     */
    public double getEdaDerivativeIntegral() {
        return getAbsDerivativeIntegral(edaDerivative);
    }

    /**
     *
     * @return
     */
    public double getEmgDerivativeIntegral() {
        return getAbsDerivativeIntegral(emgDerivative);
    }

    /**
     * Method detecting flat fragments with max value (for artifacts detection)
     * @param series time series used to count max value samples
     * @return number of samples close to the max value
     */
    public int getMaxFlatSamplesCount(int[] series) {
        int count = 0;
        for (int s : series) {
            if (s > 1020) count++;
        }
        return count;
    }

    /**
     * Method detecting flat fragments with min value (for artifacts detection)
     * @param series time series used to count min value samples
     * @return number of samples close to the min value
     */
    public int getMinFlatSamplesCount(int[] series) {
        int count = 0;
        for (int s : series) {
            if (s < 3) count++;
        }
        return count;
    }

    /**
     * Method checking amount of samples which are very close to the maximal or minimal possible value (useful for detecting artifacts)
     * @return number of samples close to the border values
     */
    public int getEcgFlatSamplesCount() {
        //this implementation is good for ECG derivative
        /*int count = 0;
        for (int s : ecgValues) {
            if (Math.abs(s) < EPSILON) count++;
        }
        return count;*/
//        return getMaxFlatSamplesCount(ecgValues)+getMinFlatSamplesCount(ecgValues);
        int count = 0;
        for (int s : ecgValues) {
            if (s > EPSILON && s < 1023-EPSILON) continue;
            count++;
        }
        return count;
    }

    /**
     * Method calculating mean and variance of peaks values in given series (low variance in EMG indicates noise)
     * @param series time series that is to be used in calculation
     * @param threshold minimal value of a peak that will be used in calculation
     * @return 1D array with mean peak value at position 0 and variance at position 1
     */
    private double[] getPeakMeanAndVar(int[] series, int threshold) {
        int[] peaksLocations = computePeaksLocation(series, threshold,0);
        int[] peaksValues = new int[peaksLocations.length];
        double mean = 0;
        double var = 0;
        for (int i=0; i<peaksLocations.length; i++) {
            peaksValues[i] = series[peaksLocations[i]];
            mean += peaksValues[i];
            var += peaksValues[i]*peaksValues[i];
        }
        //currently, mean contains the sum and var contains squares sum
        if (peaksLocations.length == 0) {
            mean = var = 0;
        }
        else {
            mean /= peaksLocations.length;
            var = var/peaksLocations.length - mean*mean;
        }
        return new double[]{mean, var};
    }

    /**
     *
     * @param threshold
     * @return
     */
    public double getEmgPeaksLocsMean(int threshold) {
        if (emgPeaksLocsMeanAndVar == null)
            emgPeaksLocsMeanAndVar = getEmgPeakPositionsMeanAndVar(threshold);
        return emgPeaksLocsMeanAndVar[0];
    }

    /**
     *
     * @param threshold
     * @return
     */
    public double getEmgPeaksLocsVar(int threshold) {
        if (emgPeaksLocsMeanAndVar == null)
            emgPeaksLocsMeanAndVar = getEmgPeakPositionsMeanAndVar(threshold);
        return emgPeaksLocsMeanAndVar[1];
    }

    /**
     *
     * @param threshold
     * @return
     */
    public double getEmgPeaksMean(int threshold) {
        if (emgPeaksMeanAndVar == null)
            emgPeaksMeanAndVar = getPeakMeanAndVar(emgValues,threshold);
        return emgPeaksMeanAndVar[0];
    }

    /**
     *
     * @param threshold
     * @return
     */
    public double getEmgPeaksVar(int threshold) {
        if (emgPeaksMeanAndVar == null)
            emgPeaksMeanAndVar = getPeakMeanAndVar(emgValues,threshold);
        return emgPeaksMeanAndVar[1];
    }

    /**
     *
     * @param threshold
     * @return
     */
    private double[] getEmgPeakPositionsMeanAndVar(int threshold) {
//        if (emgPeaksLocation == null)
            emgPeaksLocation = computePeaksLocation(emgValues, threshold,0);
        double mean = 0;
        double var = 0;
        for (int peak : emgPeaksLocation) {
            mean += peak;
            var += peak * peak;
        }
        if (emgPeaksLocation.length == 0) {
            mean = var = 0;
        }
        else {
            mean /= emgPeaksLocation.length;
            var = var/emgPeaksLocation.length - mean*mean;
        }
        return (emgPeaksLocsMeanAndVar = new double[]{mean, var});
    }

    /**
     *
     * @return
     */
    public double getEdaTrend() {
        return getSeriesTrend(edaValues);
    }

    public double getHeartRateTrend() {
        return getSeriesTrend(heartRateSamples);
    }

    /**
     *
     * @param series
     * @return
     */
    private double getSeriesTrend(int[] series) {
        int[] xArray = new int[series.length];
        for (int i=0; i<xArray.length; i++) {
            xArray[i] = i;
        }
        return new LinearRegression(xArray,series).slope();
    }

    public double getEdaZeroSamplesCount() {
        int count = 0;
        for (int s : edaValues) {
            if (s < EPSILON) count++;
        }
        return count;
    }
}
