package pl.me.bitalinoclassifier.assist;

/**
 * Created by Piotr1 on 20.02.2018.
 */

public class Filters {
    public static int[] lowPassFilter(int[] series, int dt, int RC) {
        int[] result = new int[series.length];
        for (int i=1; i<series.length; i++)
            result[i] = result[i-1] + (dt/(RC+dt))*(series[i] - result[i-1]);
        return result;
    }
    public static int[] lowPassFilter(int[] series, double alpha) {
        int[] result = new int[series.length];
        for (int i=1; i<series.length; i++)
            result[i] = (int) ((1-alpha)*series[i]+alpha*result[i-1]);
        return result;
    }
}
