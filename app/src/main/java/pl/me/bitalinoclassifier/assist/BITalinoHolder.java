package pl.me.bitalinoclassifier.assist;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.jannlab.core.CellType;
import info.plux.pluxapi.bitalino.BITalinoCommunication;
import pl.me.bitalinoclassifier.neural_network.NeuralNetwork;
import pl.me.bitalinoclassifier.receivers.ResponseReceiver;

/**
 * Created by Piotr1 on 15.02.2018.
 * Assist class containing useful fields and constants which are used in different parts of the application
 */

public class BITalinoHolder {
    public static final int SAMPLE_RATE = 100;
    public static final int TIME_WINDOW_LENGTH = 5;
    public static final int FEATURES_AMOUNT = 16;
    public static final String MAC_ADDRESS = "20:16:12:21:98:64";
    private static final String PATH_CORE = Environment.getExternalStorageDirectory().getAbsolutePath()+"/mgr/";

    public static BITalinoCommunication bitalinoCommunication;
    public static Handler handler = new Handler();
//    public static Runnable batteryStateChecking = new BatteryCheckThread();
    public static RingBufferFlipMarker buffer = new RingBufferFlipMarker(SAMPLE_RATE*10+1);
    public static ResponseReceiver receiver = new ResponseReceiver();
    public static List<NeuralNetwork> neuralNetworks = new ArrayList<>();

    /*
    CellType values explainations are available in the lib source code (in CellType.java file)
     */

    /**
     * Method initializing designed neural networks (18 nets in total). When the app starts, it tries to load networks from files.
     * In case of failure, the nets are created from the scratch.
     */
    public static void initNeuralNetworks() {
        NeuralNetwork nn;
        String networkOutput = "";
        for (int i=0; i<3; i++) {
            switch (i) {
                case 0: networkOutput = "POS"; break;
                case 1: networkOutput = "NEU"; break;
                case 2: networkOutput = "NEG"; break;
            }
            /*try {
                nn = NeuralNetwork.loadNet(PATH_CORE+"1HL_TS_"+networkOutput+".net");
            } catch (IOException | ClassNotFoundException e) {
                Log.w("initNeuralNetworks()","Could not load net "+"1HL_TS_"+networkOutput+": "+e.getMessage());
                nn = new NeuralNetwork("1HL_TS_"+networkOutput);
                nn.setInputLayer(SAMPLE_RATE*TIME_WINDOW_LENGTH*3)
                        .setHiddenLayer(SAMPLE_RATE*TIME_WINDOW_LENGTH*3/2+1, CellType.SIGMOID,true,1)
                        .setOutputLayer(1,CellType.SIGMOID,true,1);
                nn.generateAndInitNet();
            }
            neuralNetworks.add(nn);*/


            try {
                nn = NeuralNetwork.loadNet(PATH_CORE+"1HL_F_"+networkOutput+".net","1HL_F_"+networkOutput);
            } catch (IOException | ClassNotFoundException e) {
                Log.w("initNeuralNetworks()", "Could not load net " + "1HL_F_" + networkOutput + ": " + e.getMessage());
                nn = new NeuralNetwork("1HL_F_"+networkOutput);
                nn.setInputLayer(FEATURES_AMOUNT)
                        .setHiddenLayer(FEATURES_AMOUNT/2+1,CellType.SIGMOID,true,1)
                        .setOutputLayer(1,CellType.SIGMOID,true,1);
                nn.generateAndInitNet();
            }
            neuralNetworks.add(nn);

            try {
                nn = NeuralNetwork.loadNet(PATH_CORE+"1HL_LIN_F_"+networkOutput+".net","1HL_LIN_F_"+networkOutput);
            } catch (IOException | ClassNotFoundException e) {
                Log.w("initNeuralNetworks()", "Could not load net " + "1HL_LIN_F_" + networkOutput + ": " + e.getMessage());
                nn = new NeuralNetwork("1HL_LIN_F_"+networkOutput);
                nn.setInputLayer(FEATURES_AMOUNT)
                        .setHiddenLayer(FEATURES_AMOUNT/2+1,CellType.LINEAR,true,1)
                        .setOutputLayer(1,CellType.SIGMOID,true,1);
                nn.generateAndInitNet();
            }
            neuralNetworks.add(nn);

            /*try {
                nn = NeuralNetwork.loadNet(PATH_CORE+"1HL_TSF_"+networkOutput+".net");
            } catch (IOException | ClassNotFoundException e) {
                Log.w("initNeuralNetworks()", "Could not load net " + "1HL_F_" + networkOutput + ": " + e.getMessage());
                nn = new NeuralNetwork("1HL_TSF_"+networkOutput);
                nn.setInputLayer(SAMPLE_RATE*TIME_WINDOW_LENGTH*3+BITalinoHolder.FEATURES_AMOUNT)
                        .setHiddenLayer((SAMPLE_RATE*TIME_WINDOW_LENGTH*3+BITalinoHolder.FEATURES_AMOUNT)/2+1,CellType.SIGMOID,true,1)
                        .setOutputLayer(1,CellType.SIGMOID,true,1);
                nn.generateAndInitNet();
            }
            neuralNetworks.add(nn);*/


            /*try {
                nn = NeuralNetwork.loadNet(PATH_CORE+"2HL_TS_"+networkOutput+".net");
            } catch (IOException | ClassNotFoundException e) {
                Log.w("initNeuralNetworks()", "Could not load net " + "2HL_TS_" + networkOutput + ": " + e.getMessage());
                nn = new NeuralNetwork("2HL_TS_"+networkOutput);
                nn.setInputLayer(SAMPLE_RATE*TIME_WINDOW_LENGTH*3)
                        .setHiddenLayer(SAMPLE_RATE*TIME_WINDOW_LENGTH*3/2+1, CellType.SIGMOID,true,1)
                        .setHiddenLayer((SAMPLE_RATE*TIME_WINDOW_LENGTH*3/2+1)/2+1, CellType.SIGMOID, true, 1)
                        .setOutputLayer(1,CellType.SIGMOID,true,1);
                nn.generateAndInitNet();
            }
            neuralNetworks.add(nn);*/


            try {
                nn = NeuralNetwork.loadNet(PATH_CORE+"2HL_F_"+networkOutput+".net","2HL_F_"+networkOutput);
            } catch (IOException | ClassNotFoundException e) {
                Log.w("initNeuralNetworks()", "Could not load net " + "2HL_F_" + networkOutput + ": " + e.getMessage());
                nn = new NeuralNetwork("2HL_F_"+networkOutput);
                nn.setInputLayer(BITalinoHolder.FEATURES_AMOUNT)
                        .setHiddenLayer(BITalinoHolder.FEATURES_AMOUNT/2+1,CellType.SIGMOID,true,1)
                        .setHiddenLayer((BITalinoHolder.FEATURES_AMOUNT/2+1)/2+1, CellType.SIGMOID, true, 1)
                        .setOutputLayer(1,CellType.SIGMOID,true,1);
                nn.generateAndInitNet();
            }
            neuralNetworks.add(nn);

            try {
                nn = NeuralNetwork.loadNet(PATH_CORE+"2HL_LIN_F_"+networkOutput+".net","2HL_LIN_F_"+networkOutput);
            } catch (IOException | ClassNotFoundException e) {
                Log.w("initNeuralNetworks()", "Could not load net " + "2HL_LIN_F_" + networkOutput + ": " + e.getMessage());
                nn = new NeuralNetwork("2HL_LIN_F_"+networkOutput);
                nn.setInputLayer(BITalinoHolder.FEATURES_AMOUNT)
                        .setHiddenLayer(BITalinoHolder.FEATURES_AMOUNT/2+1,CellType.LINEAR,true,1)
                        .setHiddenLayer((BITalinoHolder.FEATURES_AMOUNT/2+1)/2+1, CellType.LINEAR, true, 1)
                        .setOutputLayer(1,CellType.SIGMOID,true,1);
                nn.generateAndInitNet();
            }
            neuralNetworks.add(nn);

            /*try {
                nn = NeuralNetwork.loadNet(PATH_CORE+"2HL_TSF_"+networkOutput+".net");
            } catch (IOException | ClassNotFoundException e) {
                Log.w("initNeuralNetworks()", "Could not load net " + "2HL_TSF_" + networkOutput + ": " + e.getMessage());
                nn = new NeuralNetwork("2HL_TSF_"+networkOutput);
                nn.setInputLayer(SAMPLE_RATE*TIME_WINDOW_LENGTH*3+BITalinoHolder.FEATURES_AMOUNT)
                        .setHiddenLayer((SAMPLE_RATE*TIME_WINDOW_LENGTH*3+16)/2+1,CellType.SIGMOID,true,1)
                        .setHiddenLayer(((SAMPLE_RATE*TIME_WINDOW_LENGTH*3+16)/2+1)/2+1, CellType.SIGMOID, true, 1)
                        .setOutputLayer(1,CellType.SIGMOID,true,1);
                nn.generateAndInitNet();
            }
            neuralNetworks.add(nn);*/
        }

        if (!NeuralNetwork.validateSetsSizes()) {
            Log.e("generateAndInitNet()","Training sets inconsistency!");
        }

    }
}
