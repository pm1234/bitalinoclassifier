package pl.me.bitalinoclassifier.neural_network;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;

import de.jannlab.Net;
import de.jannlab.core.CellType;
import de.jannlab.data.Sample;
import de.jannlab.data.SampleSet;
import de.jannlab.generator.MLPGenerator;
import de.jannlab.training.GradientDescent;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;

/**
 * Created by Piotr1 on 26.02.2018.
 */

public class NeuralNetwork implements Serializable{
    private String name;
    private MLPGenerator gen;
    private Net mlp;
    private GradientDescent trainer;
    private SampleSet trainingSet, testSet, normalizedTrainingSet;
    private final String TAG = "NeuralNetwork";
    private static final String PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+"/mgr/";
    private double[] featuresMin = new double[BITalinoHolder.FEATURES_AMOUNT];
    private double[] featuresMax = new double[BITalinoHolder.FEATURES_AMOUNT];
    private Random rnd = new Random(0L);

    public NeuralNetwork(String name) {
        this.name = name;
        gen = new MLPGenerator();
        for (int i=0; i<featuresMax.length; i++) {
            featuresMax[i] = 0;
            featuresMin[i] = 1;
        }
    }

    public NeuralNetwork setInputLayer(int neuronsNum) {
        gen.inputLayer(neuronsNum);
        return this;
    }

    public NeuralNetwork setHiddenLayer(int neuronsNum, CellType cellType, boolean useBias, double biasValue) {
        gen.hiddenLayer(neuronsNum, cellType, useBias, biasValue);
        return this;
    }

    public NeuralNetwork setOutputLayer (int neuronsNum, CellType cellType, boolean useBias, double biasValue) {
        gen.outputLayer(neuronsNum, cellType, useBias, biasValue);
        return this;
    }

    public void generateAndInitNet() {
        mlp = gen.generate();
        mlp.initializeWeights(new Random(0L));

        trainer = new GradientDescent();
        trainer.setNet(mlp);
        trainer.setRnd(rnd);
        trainer.setEpochs(500);
        trainer.setTargetError(0);
        trainer.setLearningRate(0.5);
        trainer.setMomentum(0.5);
        trainer.setTrainingSet(normalizedTrainingSet);

        if (name.contains("_NEG"))
            loadTrainingSet(PATH+"neg.trs");
        else if (name.contains("_NEU"))
            loadTrainingSet(PATH+"neu.trs");
        else if (name.contains("_POS"))
            loadTrainingSet(PATH+"pos.trs");
        else
            Log.e("generateAndInitNet()", "Cannot load training set for net "+name);

    }

    public void addCaseToTrainingSet(Sample sample) {
        if (trainingSet == null || trainingSet.isEmpty()) {
            if (name.contains("_NEG"))
                loadTrainingSet(PATH+"neg.trs");
            else if (name.contains("_NEU"))
                loadTrainingSet(PATH+"neu.trs");
            else if (name.contains("_POS"))
                loadTrainingSet(PATH+"pos.trs");
            else
                Log.e("generateAndInitNet()", "Cannot load training set for net "+name);
        }
        trainingSet.add(sample);
        if (name.contains("_NEG"))
            saveTrainingSet(PATH+"neg.trs");
        else if (name.contains("_NEU"))
            saveTrainingSet(PATH+"neu.trs");
        else if (name.contains("_POS"))
            saveTrainingSet(PATH+"pos.trs");
        else
            Log.e("generateAndInitNet()", "Cannot save training set for net "+name);
    }

    public void trainNetAndSaveDataset() {
        Log.d("NeuralNetwork","Training net "+name+"; training set: "+trainingSet.size()+" elements.");
//        if (trainingSet == null || trainingSet.isEmpty()) {
            if (name.contains("_NEG"))
                loadTrainingSet(PATH+"neg.trs");
            else if (name.contains("_NEU"))
                loadTrainingSet(PATH+"neu.trs");
            else if (name.contains("_POS"))
                loadTrainingSet(PATH+"pos.trs");
            else
                Log.e("generateAndInitNet()", "Cannot load training set for net "+name);
//        }
        normalizedTrainingSet = normalizeTrainingSet();
        trainer.setTrainingSet(normalizedTrainingSet);
//        mlp.initializeWeights(rnd);
        if (normalizedTrainingSet.size() % 5 == 0)
            trainer.train();

        if (name.contains("_NEG"))
            saveTrainingSet(PATH+"neg.trs");
        else if (name.contains("_NEU"))
            saveTrainingSet(PATH+"neu.trs");
        else if (name.contains("_POS"))
            saveTrainingSet(PATH+"pos.trs");
        else
            Log.e("generateAndInitNet()", "Cannot save training set for net "+name);
//        saveTrainingSet(PATH+name+"_trs.bcd");
//        Log.d("NeuralNetwork","Weights for net "+name+": "+ Arrays.toString(mlp.getWeights()));
    }

    public static boolean validateSetsSizes() {
        if(BITalinoHolder.neuralNetworks.get(0).getTrainingSet() == null) {
            Log.w("validateSetsSizes()", "Training sets not loaded yet!");
            return true;
        }
        int firstSize = BITalinoHolder.neuralNetworks.get(0).getTrainingSet().size();
        for (NeuralNetwork nn : BITalinoHolder.neuralNetworks) {
            if (nn.trainingSet.size() != firstSize) {
                Log.e("validateSetsSize()","Net "+nn.name+": training set size: "+nn.trainingSet.size()+", first size="+firstSize);
                return false;
            }
        }
        return true;
    }

    public void saveDataset() {
        Log.d("NeuralNetwork","Training net "+name+"; training set: "+trainingSet.size()+" elements.");
        if (trainingSet == null || trainingSet.isEmpty()) {
            if (name.contains("_NEG"))
                loadTrainingSet(PATH+"neg.trs");
            else if (name.contains("_NEU"))
                loadTrainingSet(PATH+"neu.trs");
            else if (name.contains("_POS"))
                loadTrainingSet(PATH+"pos.trs");
            else
                Log.e("generateAndInitNet()", "Cannot load training set for net "+name);
        }
//        normalizedTrainingSet = normalizeTrainingSet();
        saveTrainingSet(PATH+name+"_trs.bcd");
    }

    private SampleSet normalizeTrainingSet() {
        for (Sample ts : trainingSet) {
            double[] features = ts.getInput();
            for (int i=0; i<features.length; i++) {
                featuresMin[i] = Math.min(featuresMin[i], features[i]);
                featuresMax[i] = Math.max(featuresMax[i], features[i]);
            }
        }
//        double[] normalizedCase = new double[BITalinoHolder.FEATURES_AMOUNT];
        normalizedTrainingSet = new SampleSet();
        for (Sample ts : trainingSet) {
//            double[] features = ts.getInput();
//            for (int i=0; i<features.length; i++) {
//                normalizedCase[i] = (features[i] - featuresMin[i])/(featuresMax[i] - featuresMin[i]);
//            }
//            normalizedTrainingSet.add(new Sample(normalizedCase,ts.getTarget()));
            normalizedTrainingSet.add(normalizeCase(ts));
        }
        return normalizedTrainingSet;
    }

    public double testWithLiveData(Sample testCase) {
        Log.d("NeuralNetwork","Test case: "+testCase.toString());
        double[] out = new double[1];
        Sample normalizedTestCase = normalizeCase(testCase);
        Log.d("NeuralNetwork","Normalized test case: "+normalizedTestCase.toString());
        normalizedTestCase.mapInput(mlp.inputPort());
//        mlp.reset();
        mlp.compute();
        mlp.output(out,0);
        return out[0];
    }

    private Sample normalizeCase(Sample sample) {
        double[] normalizedCase = new double[BITalinoHolder.FEATURES_AMOUNT];
        double[] features = sample.getInput();
        for (int i=0; i<features.length; i++) {
            if (featuresMax[i] - featuresMin[i] == 0) {
                normalizedCase[i] = 0; //such a feature gives no information
            }
            else
                normalizedCase[i] = (features[i] - featuresMin[i])/(featuresMax[i] - featuresMin[i]);
            //0/0 case caused NaN to be present in the set, thus disabling proper learning
        }
        return new Sample(normalizedCase,sample.getTarget());
    }

    public SampleSet getTrainingSet() {
        return trainingSet;
    }

    public void setTrainingSet(SampleSet trainingSet) {
        this.trainingSet = trainingSet;
    }

    private boolean loadTrainingSet(String fileName) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
            trainingSet = (SampleSet) ois.readObject();
            ois.close();
            return true;
        } catch ( IOException | ClassNotFoundException e) {
            Log.w(TAG,"Error while loading training set for the network " + name + ": " + e.getMessage());
            if (trainingSet == null) trainingSet = new SampleSet();
            return false;
        }
    }

    public boolean saveTrainingSet(String fileName) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
            oos.writeObject(trainingSet);
            oos.close();
            return true;
        } catch (IOException e) {
            Log.w(TAG,"Error while saving training set for the network "+name+": "+e.getMessage());
            return false;
        }
    }

    public boolean saveNet(String fileName) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
            oos.writeObject(mlp);
            oos.close();
            oos = new ObjectOutputStream(new FileOutputStream(fileName+".normmin"));
            oos.writeObject(featuresMin);
            oos.close();
            oos = new ObjectOutputStream(new FileOutputStream(fileName+".normmax"));
            oos.writeObject(featuresMax);
            oos.close();
            return true;
        } catch (IOException e) {
            Log.w(TAG,"Error while saving network "+name+": "+e.getMessage());
            return false;
        }
    }

    public boolean saveNetWeights(String fileName) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
            bw.write(Arrays.toString(mlp.getWeights()));
            bw.close();
            return true;
        } catch (IOException e) {
            Log.w(TAG,"Error while saving net weights for the network "+name+": "+e.getMessage());
            return false;
        }
    }

    public static NeuralNetwork loadNet(String fileName, String netName) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
        Net mlp = (Net) ois.readObject();
        ois.close();
        ois = new ObjectInputStream(new FileInputStream(fileName+".normmin"));
        double[] featuresMin = (double[]) ois.readObject();
        ois.close();
        ois = new ObjectInputStream(new FileInputStream(fileName+".normmax"));
        double[] featuresMax = (double[]) ois.readObject();
        ois.close();
//        Log.d("loadNet()", Arrays.toString(featuresMin));
//        Log.d("loadNet()", Arrays.toString(featuresMax));
        for (int i=0; i<featuresMax.length; i++) {
            if (featuresMin[i] > featuresMax[i]) {
                Log.e("loadNet()", "i="+String.valueOf(i)+", min="+String.valueOf(featuresMin[i])+", max="+String.valueOf(featuresMax[i]));
            }
        }
        NeuralNetwork nn = new NeuralNetwork(netName);
        nn.setMlp(mlp);
        nn.setFeaturesMin(featuresMin);
        nn.setFeaturesMax(featuresMax);
        GradientDescent trainer = new GradientDescent();
        Random rnd = new Random(0L);
        trainer.setNet(mlp);
        trainer.setRnd(rnd);
        trainer.setEpochs(500);
        trainer.setTargetError(0);
        trainer.setLearningRate(0.5);
        trainer.setMomentum(0.5);
        nn.setTrainer(trainer);
//        trainer.setTrainingSet(trainingSet);

//        loadTrainingSet(PATH+netName+"_trs.bcd");
        return nn;
    }

    public boolean loadTestSet(String fileName) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
            testSet = (SampleSet) ois.readObject();
            ois.close();
            return true;
        } catch ( IOException | ClassNotFoundException e) {
            Log.w(TAG,"Error while loading test set for the network " + name + ": " + e.getMessage());
            if (testSet == null) testSet = new SampleSet();
            return false;
        }
    }

    public boolean saveTestSet(String fileName) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
            oos.writeObject(testSet);
            oos.close();
            return true;
        } catch (IOException e) {
            Log.w(TAG,"Error while saving test set for the network "+name+": "+e.getMessage());
            return false;
        }
    }

    public SampleSet getTestSet() {return testSet;}

    public String getName() {return name;}

    public void setMlp(Net mlp) {
        this.mlp = mlp;
    }

    public void setTrainer(GradientDescent trainer) {
        this.trainer = trainer;
    }

    public void setFeaturesMin(double[] featuresMin) {
        this.featuresMin = featuresMin;
    }

    public void setFeaturesMax(double[] featuresMax) {
        this.featuresMax = featuresMax;
    }
}
