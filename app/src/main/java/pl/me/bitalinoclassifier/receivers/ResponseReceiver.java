package pl.me.bitalinoclassifier.receivers;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import info.plux.pluxapi.Constants;
import info.plux.pluxapi.bitalino.BITalinoDescription;
import info.plux.pluxapi.bitalino.BITalinoState;
import pl.me.bitalinoclassifier.assist.BITalinoHolder;
import pl.me.bitalinoclassifier.assist.BatteryCheckThread;
import pl.me.bitalinoclassifier.fragments.Tab1;

import static android.content.ContentValues.TAG;

/**
 * Created by Piotr1 on 15.02.2018.
 */

public class ResponseReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
//        String text = intent.getStringExtra(BITalinoConnectingService.PARAM_OUT_MSG);
//        Toast.makeText(context,"Received: "+text,Toast.LENGTH_SHORT).show();
        final String action = intent.getAction();

        System.out.print("Action received: "+action);

        if (Constants.ACTION_STATE_CHANGED.equals(action)) {
            String identifier = intent.getStringExtra(Constants.IDENTIFIER);
            Constants.States state =
                    Constants.States.getStates(intent.getIntExtra(Constants.EXTRA_STATE_CHANGED,0));
            System.out.println(" New state: "+state.name());
            Log.i(TAG, "Device " + identifier + ": " + state.name());

            //Processing a successful connection:
            if (state.name().equals("CONNECTED")) {
                Tab1.acquisitionToggleButton.setEnabled(true);
                Tab1.toggleButton.setEnabled(true);
                Toast.makeText(context,"Connection established",Toast.LENGTH_LONG).show();
                BITalinoHolder.handler.postDelayed(new BatteryCheckThread(),500);
            }

            //Procesing a successful starting the acquisition process:
            else if (state.name().equals("ACQUISITION_OK")) {
                Tab1.acquisitionToggleButton.setEnabled(true);
                Toast.makeText(context,"Acquisition started",Toast.LENGTH_LONG).show();
//                BITalinoHolder.handler.removeCallbacks(BITalinoHolder.batteryStateChecking);
            }

            else if (state.name().equals("DISCONNECTED")) {
                Tab1.acquisitionToggleButton.setChecked(false);
                Tab1.acquisitionToggleButton.setEnabled(false);
                Tab1.toggleButton.setEnabled(true);
                Tab1.toggleButton.setChecked(false);
//                BITalinoHolder.handler.removeCallbacks(BITalinoHolder.batteryStateChecking);
                Tab1.batteryStatus.setProgress(0);
                Toast.makeText(context,"Disconnected",Toast.LENGTH_LONG).show();
            }
            else if (state.name().equals("NO_CONNECTION")) {
                Tab1.batteryStatus.setProgress(0);
                Tab1.acquisitionToggleButton.setChecked(false);
                Tab1.acquisitionToggleButton.setEnabled(false);
                Tab1.toggleButton.setEnabled(true);
                Tab1.toggleButton.setChecked(false);
                Toast.makeText(context,"No connection",Toast.LENGTH_LONG).show();
            }

        }
        else if (Constants.ACTION_DATA_AVAILABLE.equals(action)) {
//            BITalinoFrame frame = intent.getParcelableExtra(Constants.EXTRA_DATA);
//            Log.d(TAG, "BITalinoFrame: " + frame.toString());
        }
        else if (Constants.ACTION_COMMAND_REPLY.equals(action)) {
            String identifier = intent.getStringExtra(Constants.IDENTIFIER);
            Parcelable parcelable = intent.getParcelableExtra(Constants.EXTRA_COMMAND_REPLY);
            if(parcelable.getClass().equals(BITalinoState.class)){
                Log.d(TAG, "BITalinoState: " + parcelable.toString());

                //battery state is here
                int batIndex = parcelable.toString().indexOf("Battery");
                int semicolonIndex = parcelable.toString().indexOf(';',batIndex);
                int batteryValue = Integer.parseInt(parcelable.toString().substring(batIndex+9,semicolonIndex));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Tab1.batteryStatus.setProgress(batteryValue-500,true);
                }
                else { Tab1.batteryStatus.setProgress(batteryValue-500); }
            } else if(parcelable.getClass().equals(BITalinoDescription.class)){
                Log.d(TAG, "BITalinoDescription: isBITalino2: " +
                        ((BITalinoDescription)parcelable).isBITalino2() + "\"; FwVersion: "+
                String.valueOf(((BITalinoDescription)parcelable).getFwVersion()));
            }
        }
        else if (Constants.ACTION_MESSAGE_SCAN.equals(action)){
            BluetoothDevice device = intent.getParcelableExtra(Constants.EXTRA_DEVICE_SCAN);
        }
    }
}
